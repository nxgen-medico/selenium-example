import { Builder } from "selenium-webdriver";

const server = process.env.SELENIUM_SERVER;
const driver = new Builder().forBrowser("chrome").usingServer(server).build();
await driver.get('https://www.selenium.dev/selenium/web/web-form.html');
const title = await driver.getTitle();

console.log(`The title is: ${title}`);


await driver.close();
