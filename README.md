# Using Selenium with GitLab CI

This is an example repository that contains a very basic NodeJS project that is getting the title of a web page using Selenium.
It's using a standalone Docker image with Chrome.

Just take a look at the `.gitlab-ci.yml` file ; you will need to adjust the `image` to one to run your environment (a `python` image if you are using Python for example), and then adjust the `script` section to call your project.

In this example, it's also using a `SELENIUM_SERVER` environment variable.
This contains the value that is needed for the app to reach Selenium.
You can hard-code it in your app, but this is a bad practice.
Environment variables should be used instead for such configuration parameters.
